package com.developer.techies.suregiftandroidtest.common.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;

public class ListOfHotelsHolder extends RecyclerView.ViewHolder{

    public ImageView hotelImage;
    public TextView hotelAddress;
    public TextView hotelPrice;

    public ListOfHotelsHolder(View view){
        super(view);

        hotelImage = view.findViewById(R.id.hotel_image);
        hotelAddress = view.findViewById(R.id.hotel_address);
        hotelPrice = view.findViewById(R.id.hotel_price);
    }
}
