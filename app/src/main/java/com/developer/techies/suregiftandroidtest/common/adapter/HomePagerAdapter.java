package com.developer.techies.suregiftandroidtest.common.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.developer.techies.suregiftandroidtest.common.view.fragment.ExploreCityFragment;
import com.developer.techies.suregiftandroidtest.common.view.bottom_fragment.HomeFragment;
import com.developer.techies.suregiftandroidtest.common.view.fragment.PopularFragment;
import com.developer.techies.suregiftandroidtest.common.view.fragment.ViewMapFragment;

public class HomePagerAdapter extends FragmentPagerAdapter {

    public HomePagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new HomeFragment();
            case 1:
                return new ExploreCityFragment();
            case 2:
                return new PopularFragment();
            case 3:
                return new ViewMapFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Near Me";
            case 1:
                return "Explore City";
            case 2:
                return "Popular";
            case 3:
                return "View Map";
        }
        return "";
    }
}