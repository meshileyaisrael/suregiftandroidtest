package com.developer.techies.suregiftandroidtest.util.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;

public class PickerItemHolder extends RecyclerView.ViewHolder {

    public TextView mItemView;

    public PickerItemHolder(View view){
        super(view);
        mItemView = (TextView) view.findViewById(R.id.list_item);
    }
}