package com.developer.techies.suregiftandroidtest.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.parse.ParseException;

import java.net.UnknownHostException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class BaseActivity extends AppCompatActivity {

    public void presentError(Exception e) {
        hideProgress();
    }

    public void complete(String message) {
        complete(message, null);
    }

    public void complete(String message, Intent intent) {
        //Show the message as a dialog. Then when the user clicks on the dialog to dismiss.
        //Do a
        //setResult(Intent.RESULT_OK)
        //finish();

        NotificationUtil.notifyDialog(this, true, "Success", message, "OK", v -> {
            if (getCallingActivity() != null) {
                if (intent != null)
                    setResult(RESULT_OK, intent);
                else
                    setResult(RESULT_OK);
                finish();
            } else {
                finish();
            }
        });
    }

    public static void LaunchClass(Context context) {
        Class klass = getNavigationClass(User.getCurrentUser());
        LaunchClass(context, klass, null, true);
    }

    public static void LaunchClass(Context context, Class<?> cla, Bundle bundle, boolean isNewTask) {
        Intent intent = new Intent(context, cla);
        if (bundle != null)
            intent.putExtras(bundle);
        if (isNewTask)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        //context.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private static Class<?> getNavigationClass(User user) {
        if (user == null) {
        }
        return null;
    }


    public static void LaunchClass(Activity context, Class<?> cla, Bundle bundle, boolean isNewTask) {
        Intent intent = new Intent(context, cla);
        if (bundle != null)
            intent.putExtras(bundle);
        if (isNewTask)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        context.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initBar("");
    }

    public void initTitle(String title, String subTitle) {
        TextView titleView = findViewById(R.id.page_title);
        TextView subTitleView = findViewById(R.id.page_sub_title);

        if (titleView != null && title != null)
            titleView.setText(title);
        if (subTitleView != null && subTitle != null)
            subTitleView.setText(subTitle);
    }

    public void setContentView(int layoutResID, String title) {
        super.setContentView(layoutResID);
        initBar(title);
    }


    private boolean isProgressShowing = false;
    private boolean isProgressCancellable = false;

    public void initBar(String title) {
        initBar(title, true);
    }

    public void initBar(String title, boolean backButton) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            if (backButton && !isTaskRoot())
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(1);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public Fragment getFragmentFromPager(@IdRes int pagerId, int index) {
        return getSupportFragmentManager().findFragmentByTag("android:switcher:" + pagerId + ":" + index);
    }

    public void showErrorInformation(Exception ex) {

        if (ex == null) {
            //Nothing to be captured really
            return;
        }

        if (ex instanceof ParseException) {
            ParseException parseException = (ParseException) ex;
            int errorCode = parseException.getCode();
            Log.e("TAG", "The ERROR CODE:::: " + errorCode);
            if (errorCode == ParseException.OBJECT_NOT_FOUND) {
                NotificationUtil.notifyDialog(this, false, "ERROR", "An unknown error occured, please try again");
            } else {
                NotificationUtil.notifyDialog(this, false, "INVALID ", "NETWORK ERROR, PLEASE TRY AGAIN");
            }
        } else {
            String message = ex.getMessage();
            if (message != null) {
                showErrorInformation(message);
            }
        }
    }

    public void showAuthErrorInformation(Exception ex) {

        if (ex == null) {
            //Nothing to be captured really
            return;
        }

        if (ex instanceof ParseException) {
            ParseException parseException = (ParseException) ex;
            int errorCode = parseException.getCode();
            Log.e("TAG", "The ERROR CODE:::: " + errorCode);
            if (errorCode == ParseException.INVALID_SESSION_TOKEN) {
                NotificationUtil.notifyDialog(this, false, getBaseContext().getString(R.string.invalid_token), "Please click to continue", (v) -> {
                    LogoutHelper.Logout(this);
                });
            } else if (errorCode == ParseException.INVALID_EMAIL_ADDRESS) {
                NotificationUtil.notifyDialog(this, false, "INVALID", "You have provided an invalid email address");
            } else if (errorCode == ParseException.CONNECTION_FAILED) {
                NotificationUtil.notifyDialog(this, false, "NETWORK ERROR", "Network error, Please try again later");
            } else if (errorCode == ParseException.EMAIL_NOT_FOUND) {
                NotificationUtil.notifyDialog(this, false, "INVALID", "The specified email address doesn't exist.");
            } else if (errorCode == ParseException.INVALID_EMAIL_ADDRESS) {
                NotificationUtil.notifyDialog(this, false, "INVALID ", "YOu have provided an invalid email address. Please check again");
            } else if (errorCode == ParseException.USERNAME_TAKEN || errorCode == ParseException.EMAIL_TAKEN) {
                NotificationUtil.notifyDialog(this, false, "INVALID", "THIS EMAIL IS ALREADY REGISTERED WITH US, PLEASE TRY WITH ANOTHER USERNAME");
            } else if (errorCode == ParseException.CONNECTION_FAILED) {
                NotificationUtil.notifyDialog(this, false, "INVALID ", "Network error please try again later");
            } else if (errorCode == ParseException.INVALID_LINKED_SESSION) {
                NotificationUtil.notifyDialog(this, false, "INVALID ", "INVALID SESSION TOKEN");
            } else if (errorCode == ParseException.OBJECT_NOT_FOUND) {
                NotificationUtil.notifyDialog(this, false, "INVALID LOGIN", "Invalid username or password. Please check through and try again");
            } else {
                NotificationUtil.notifyDialog(this, false, "INVALID ", "NETWORK ERROR, PLEASE TRY AGAIN");
            }
        } else {
            String message = ex.getMessage();
            if (message != null) {
                showErrorInformation(message);
            }
        }
    }

    public void initEmpty(@DrawableRes int imageRes, String message) {
        View view = findViewById(R.id.empty);
        if (view == null) {
            view = LayoutInflater.from(this).inflate(R.layout.empty, null, true);
        }
        view.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        TextView mTextView = view.findViewById(R.id.text);
        mTextView.setText(message);
        ImageView mImageView = view.findViewById(R.id.image);
        mImageView.setImageResource(imageRes);
        ((ViewGroup) findViewById(android.R.id.content)).removeView(view);
        ((ViewGroup) findViewById(android.R.id.content)).addView(view);
    }

    public void showEmpty(boolean show) {
        View view = findViewById(R.id.empty);
        if (show) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    public void hideEmptyView() {
        View view = findViewById(R.id.empty);
        if (view != null) {
            ((ViewGroup) findViewById(android.R.id.content)).removeView(view);
            view.setVisibility(View.GONE);
        } else {
            Log.e("Util", "The empty value couldn't be hidden because it was actually null");
        }
    }

    public void showErrorInformation(Throwable t) {
        if (t instanceof UnknownHostException) {
            showErrorInformation("Kindly check your internet connectivity and try again.");
            return;
        }

        String message = t.getMessage();
        if (message != null) {
            showErrorInformation(message);
        } else {
            showErrorInformation("An error has occurred. Please try again later");
        }
    }

    public void showMessage(String message, boolean isError) {
        final ViewGroup viewGroup = findViewById(android.R.id.content);
        final View messageView = LayoutInflater.from(this).inflate(R.layout.layout_message, null, true);
        if (!isError) {
            messageView.setBackgroundColor(getResources().getColor(R.color.success_green));
        }
        TextView mLabelTV = messageView.findViewById(R.id.message_textview);
        mLabelTV.setText(message);
        messageView.findViewById(R.id.close_icon).setOnClickListener(view -> messageView.setVisibility(View.GONE));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        viewGroup.addView(messageView, params);
        messageView.setVisibility(View.VISIBLE);
    }

    public void showErrorInformation(String message) {
        showMessage(message, true);
    }

    public void showSuccessInformation(String message) {
        showMessage(message, false);
    }

    public void replaceFragment(@IdRes int viewId, Fragment fragment, String backState) {
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backState, 0);
        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(viewId, fragment, backState);
            if (backState != null)
                ft.addToBackStack(backState);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {

        if (isProgressShowing && isProgressCancellable) {
            hideProgress();
            return;
        } else if (isProgressShowing) {

        } else {
            super.onBackPressed();
        }

    }

    public void showProgress(String message) {
        this.showProgress(message, false);
    }

    public void showProgress(String message, boolean isCancellable) {
        final ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView();

        final View progressView = LayoutInflater.from(this).inflate(R.layout.progress_view, null, true);
        progressView.setTag(780);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewGroup.addView(progressView, params);
        progressView.setVisibility(View.VISIBLE);

        TextView textView = progressView.findViewById(R.id.message);
        if (message != null && textView != null) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(message);
        }

        this.isProgressShowing = true;
        this.isProgressCancellable = isCancellable;
    }

    public void hideProgress() {

        final ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView();
        View progressView = viewGroup.findViewWithTag(780);
        if (progressView != null) {
            progressView.setVisibility(View.GONE);
            viewGroup.removeView(progressView);
        }

        isProgressShowing = false;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermission(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            requestPermissions(permissions, requestCode);
    }
}
