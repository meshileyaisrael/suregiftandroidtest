package com.developer.techies.suregiftandroidtest.db.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseUser;

import java.util.Date;
import java.util.List;

@ParseClassName("_User")
public class User extends ParseUser {

    public static User getCurrentUser() {
        try {
            return (User) ParseUser.getCurrentUser();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void setRole(String role) {
        put("Role", role);
    }
    public String getRole() {
        return getString("Role");
    }

    public void setLocation(String location){
        put("location", location);
    }

    public String getLocation(){
        return getString("location");
    }

    public void setOnline(boolean online) {
        put("online", online);
    }

    public boolean isOnline() {
        return getBoolean("online");
    }

    public String getFullName() {
        if (getFirstName() != null && getLastName() != null)
            return getFirstName().trim() + " " + getLastName().trim();
        else if (getFirstName() != null)
            return getFirstName().trim();
        else return getLastName() != null ? getLastName().trim() : "NULL";
    }

    public String getFirstName() {
        return getString("firstName");
    }

    public void setFirstName(String firstName) {
        put("firstName", firstName);
    }

    public String getLastName() {
        return getString("lastName");
    }

    public void setAboutMe(String aboutMe) {
        put("aboutMe", aboutMe);
    }

    public String getAboutMe() {
        return getString("aboutMe");
    }

    public void setFacebook(String facebook) {
        put("facebookHandle", facebook);
    }

    public String getFacebookHandle() {
        return getString("facebookHandle");
    }

    public void setTwitterHandle(String twitterHandle) {
        put("twitterHandle", twitterHandle);
    }

    public String getTwitterHandle() {
        return getString("twitterHandle");
    }

    public void setInstagramHandle(String instagramHandle) {
        put("instagramHandle", instagramHandle);
    }

    public String getInstagramHandle() {
        return getString("instagramHandle");
    }


    public void setLastName(String lastName) {
        put("lastName", lastName);
    }

    public Date getDateOfBirth() {
        return getDate("dateOfBirth");
    }

    public void setDateOfBirth(Date date) {
        put("dateOfBirth", date);
    }

    public String getPhoneNumber() {
        if (has("phoneNumber")) {
            String phone = getString("phoneNumber");
            return phone.replace(" ", "");
        } else {
            return null;
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        put("phoneNumber", phoneNumber);
    }

    public void setSex(String value) {
        put("sex", value);
    }

    public String getSex() {
        return getString("sex");
    }

    public void setPhoto(ParseFile file) {
        put("photo", file);
    }

    public void setFacebookPhotoURL(String url) {
        put("facebookPhoto", url);
    }

    public void setGooglePhotoURL(String url) {
        put("googlePhoto", url);
    }

    public String getPhoto() {
        if (has("photo")) {
            ParseFile file = getParseFile("photo");
            if (file == null)
                return null;
            if (file.isDataAvailable()) {
                return file.getUrl();
            }
            return null;
        } else if (has("facebookPhoto")) {
            return getString("facebookPhoto");
        } else if (has("googlePhoto")) {
            return getString("googlePhoto");
        } else {
            return null;
        }
    }

    private void setLastSeen(Date date) {
        put("lastSeen", date);
    }

    public void updateLastLoginAsync() {
        setLastSeen(new Date());
        saveInBackground();
    }

}