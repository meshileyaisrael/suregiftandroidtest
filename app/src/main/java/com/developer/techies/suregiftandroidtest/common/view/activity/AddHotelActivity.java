package com.developer.techies.suregiftandroidtest.common.view.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.common.view.activity.googlesearch.SearchActivity;
import com.developer.techies.suregiftandroidtest.common.presenter.AddHotelPresenter;
import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.PickImage;
import com.developer.techies.suregiftandroidtest.util.PickerDialogFragment;
import com.developer.techies.suregiftandroidtest.util.listener.ImagePickerCallback;
import com.developer.techies.suregiftandroidtest.util.worker.SavingWorker;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

public class AddHotelActivity extends PickImage implements ImagePickerCallback {

    AddHotelPresenter presenter;
    ImageView imageView;
    EditText locationField;
    EditText paymentTime;
    HotelItems hotelItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hotel);

        initBar("Add Hotel");

        initViews();
        initData();
    }

    public void initViews() {
        imageView = findViewById(R.id.hotel_image);
        paymentTime = findViewById(R.id.hotel_payment_time);
        locationField = findViewById(R.id.hotel_location);
    }

    public void onPaymentClick(View view){
        PickerDialogFragment fragment = PickerDialogFragment.newInstance("Select an option", new String[]{"Weekly", "Monthly", "Yearly"});
        fragment.attachListener(index -> {
            if (index == 0) {
                paymentTime.setText("Weekly");
            } else if (index ==1) {
                paymentTime.setText("Monthly");
            }else{
                paymentTime.setText("Yearly");
            }
        });
        fragment.show(getSupportFragmentManager(), null);

    }

    public void onSearchCall(View view) {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, 1);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    public void initData() {
        presenter = new AddHotelPresenter(findViewById(android.R.id.content));
        this.attachImagePickerListener(this);

    }

    public void onSearchCall2(View view) {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivityForResult(intent, 280);
    }

    public void photoOnClick(View view) {
        launchPicker();

    }

    public void onSubmitClick(View view) {
        if (this.presenter.isValid() && locationField.getText().toString().length() > 0) {
            User user = User.getCurrentUser();
            hotelItems = this.presenter.getUpdate();
            hotelItems.setCreator(user);

            if (getParseFile() != null)
                hotelItems.setHotelImage(getParseFile());
            hotelItems.setHotelAddress(locationField.getText().toString());
            hotelItems.setPaymentTime(paymentTime.getText().toString());
            showProgress("Creating Hotel...");

            SavingWorker<HotelItems> worker = new SavingWorker<>(getLifecycle());
            worker.attachListener(ex -> onSaveComplete(ex));
            worker.execute(hotelItems);
        }
    }

    private void onSaveComplete(Exception ex) {
        hideProgress();
        if (ex == null) {
            if (getCallingActivity() != null) {
                Intent intent = new Intent();
//                setResult(RESULT_OK);
                complete("Hotel Added");
//                finish();
            }
        } else {
            ex.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                locationField.setText(place.getAddress().toString());

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.e("Tag", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onPickerCancelled() {
        Log.w("I am here", "HERE222");
    }

    @Override
    public void onPickerDone(Bitmap scaledBitmap) {
        Log.w("I am here", "HERE");
        imageView.setImageBitmap(scaledBitmap);
        presenter.getmPhotoView().setImageBitmap(scaledBitmap);

    }
}
