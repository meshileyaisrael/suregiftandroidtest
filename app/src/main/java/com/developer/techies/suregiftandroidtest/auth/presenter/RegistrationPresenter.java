package com.developer.techies.suregiftandroidtest.auth.presenter;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.FormViewPresenter;
import com.developer.techies.suregiftandroidtest.util.ValidatorUtil;

public class RegistrationPresenter extends FormViewPresenter<User> {

    private TextView titleTv;
    private TextView mUserName;
    private EditText mContactField;
    private EditText mEmailField;
    private EditText mPasswordField;
   // private TextView mRegisterTerms;
    private TextView mAddress;
    private TextInputLayout mPasswordLayout;

    public RegistrationPresenter(View view) {
        super(view);
        mUserName = view.findViewById(R.id.first_name);
        mContactField = view.findViewById(R.id.mobile_number);
        mEmailField = view.findViewById(R.id.email_field);
        mPasswordField = view.findViewById(R.id.password_field);
        mPasswordLayout = view.findViewById(R.id.password_layout);
        mPasswordLayout.setPasswordVisibilityToggleEnabled(true);

    }

    public void setRole(String userType) {

    }

    @Override
    public boolean isValid() {
        return (ValidatorUtil.isValid(mPasswordField, 5)
                & ValidatorUtil.isValid(mContactField) &
                ValidatorUtil.isValid(mEmailField));
    }

    @Override
    public User getUpdate() {

        User user = new User();

        user.setLastName(mUserName.getText().toString());
        user.setPhoneNumber(mContactField.getText().toString());
        user.setUsername(mUserName.getText().toString());
        user.setEmail(mEmailField.getText().toString());
        user.setPassword(mPasswordField.getText().toString());

        return user;
    }

    public TextView getTitleTv() {
        return titleTv;
    }

    public TextView getmUserName() {
        return mUserName;
    }

    public EditText getmContactField() {
        return mContactField;
    }

    public EditText getmEmailField() {
        return mEmailField;
    }

    public EditText getmPasswordField() {
        return mPasswordField;
    }
}
