package com.developer.techies.suregiftandroidtest.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.parse.ParseUser;

import java.util.Date;

public class PreferenceHelper {

    //Am setting the PrefName to the username of the logged in user. To eliminate ethe need for data dissolution on login login or dispatch_menu_security
    public static final String MAIN_PREF_NAME = ParseUser.getCurrentUser().getEmail();
    private static final String TIMELINES_NAME = "Timelines";
    private static final String REMINDER_UPDATE_NAME = "ReminderUpdate";
    public static final String TAG = "PreferenceHelper";

    public static Date getTimelinesLastUpdate(Context context) {
        // Restore preferences
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        long date = settings.getLong(TIMELINES_NAME, 0L);
        if (date == 0L)
            return null;
        else
            return new Date(date);
    }

    public static void setTimelinesLastUpdate(Context context, long key) {
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(TIMELINES_NAME, key);
        // Commit the edits!
        editor.commit();
    }


    public static Date getLastReminderUpdate(Context context) {
        // Restore preferences
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        long date = settings.getLong(REMINDER_UPDATE_NAME, 0L);
        if (date == 0L)
            return null;
        else
            return new Date(date);
    }

    public static void setLastReminderUpdate(Context context, long key) {
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(REMINDER_UPDATE_NAME, key);
        // Commit the edits!
        editor.commit();
    }


    public static Date getDateUpdate(String key) {
        Context context = ParseApplication.getInstance().getBaseContext();
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        long date = settings.getLong(key, 0L);
        if (date == 0L)
            return null;
        else
            return new Date(date);
    }

    public static void setDateUpdate(String key, Date date) {
        if (key != null)
            setDateUpdate(key, date.getTime());
    }

    public static void setDateUpdate(String key, long value) {
        Context context = ParseApplication.getInstance().getBaseContext();
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        // Commit the edits!
        editor.commit();
    }

    public static Boolean getBooleanUpdate(Context context, String key) {
        // Restore preferences
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        return settings.getBoolean(key, false);
    }

    public static void setBooleanUpdate(Context context, String key, boolean value) {
        if (context == null)
            context = ParseApplication.getInstance().getBaseContext();
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        // Commit the edits!
        editor.commit();
    }

    public static String getStringUpdate(Context context, String key) {
        // Restore preferences
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        return settings.getString(key, "");
    }

    public static void setStringUpdate(Context context, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(MAIN_PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        // Commit the edits!
        editor.commit();
    }


}
