package com.developer.techies.suregiftandroidtest.util;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.developer.techies.suregiftandroidtest.db.model.BaseObject;
import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class ParseApplication extends Application {

    private static ParseApplication sInstance;
    private static String TAG = "ParseApplication";

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
        setFont();

        Log.d(TAG, "initializing with keys");
        // Add your initialization code here
        registerParseClass();

        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId(Config.PARSE_APPLICATION_ID)
                .clientKey(Config.PARSE_CLIENT_KEY)
                .enableLocalDataStore() //Enable local storage
                .server(Config.getServerURL())   // '/' important after 'parse'
                .build());
        ParseFacebookUtils.initialize(this);
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
            }
        });

        //Parse.initialize(this, PARSE_APPLICATION_ID, PARSE_CLIENT_KEY);
        // This allows read access to all objects

        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

    }

    private void setFont() {
//        String fontPath = getApplicationContext().getString(R.string.font_regular);
//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath(fontPath)
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );
    }

    private void registerParseClass() {
        ParseObject.registerSubclass(User.class);
        ParseObject.registerSubclass(HotelItems.class);

    }

    public static ParseApplication getInstance() {
        return sInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}

