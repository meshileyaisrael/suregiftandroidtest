package com.developer.techies.suregiftandroidtest.util;

import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.util.NotificationUtil;

public class BaseFragment extends Fragment {

    public void initEmptyState(@DrawableRes int imageRes, String message, Boolean downArrow) {
        if (getView() != null) {
            View view = getView().findViewById(R.id.empty);
            if (view == null) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.empty, null, true);
                view.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                TextView mTextView = view.findViewById(R.id.text);
                mTextView.setText(message);
                ImageView mImageView = view.findViewById(R.id.image);
                mImageView.setImageResource(imageRes);
//                ImageView arrowImg = view.findViewById(R.id.arrow_img);
//                ImageView topArrowImg = view.findViewById(R.id.top_arrow_img);
//                if (downArrow == null)
//                    arrowImg.setVisibility(View.GONE);
//                else if (downArrow == true)
//                    arrowImg.setImageResource(R.drawable.bottom_right_arrow);
//                else
//                    topArrowImg.setImageResource(R.drawable.top_center_arrow);
                ((ViewGroup) getView()).addView(view);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    public void showEmpty(boolean show) {
        View view = getView() != null ? getView().findViewById(R.id.empty) : null;
        if (show) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    public void hideEmptyView() {
        View view = getView() != null ? getView().findViewById(R.id.empty) : null;
        if (view != null) {
            Log.e("Util", "hideEmptyView was actually called");
            view.setVisibility(View.GONE);
        } else {
            Log.e("Util", "The empty value couldn't be hidden because it was actually null");
        }
    }

    public void showProgress(String message) {
        this.showProgress(message, false);
    }

    private boolean isProgressShowing = false;
    private boolean isProgressCancellable = false;

    public void showProgress(String message, boolean isCancellable) {
        final ViewGroup viewGroup = (ViewGroup) getActivity().getWindow().getDecorView();
        final View progressView = LayoutInflater.from(getActivity()).inflate(R.layout.progress_view, null, true);
        progressView.setTag(780);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewGroup.addView(progressView, params);
        progressView.setVisibility(View.VISIBLE);
        TextView textView = progressView.findViewById(R.id.message);
        if (message != null && textView != null) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(message);
        }
        this.isProgressShowing = true;
        this.isProgressCancellable = isCancellable;
    }

    public void hideProgress() {
        final ViewGroup viewGroup = (ViewGroup) getActivity().getWindow().getDecorView();
        View progressView = viewGroup.findViewWithTag(780);
        if (progressView != null) {
            progressView.setVisibility(View.GONE);
            viewGroup.removeView(progressView);
        }
        isProgressShowing = false;
    }

    public void showErrorInformation(String message) {
        showMessage(message, true);
    }

    public void showMessage(String message, boolean isError) {
        final ViewGroup viewGroup = getView().findViewById(android.R.id.content);
        final View messageView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_message, null, true);
        if (!isError) {
            messageView.setBackgroundColor(getResources().getColor(R.color.success_green));
        }
        TextView mLabelTV = messageView.findViewById(R.id.message_textview);
        mLabelTV.setText(message);
        messageView.findViewById(R.id.close_icon).setOnClickListener(views -> messageView.setVisibility(View.GONE));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        viewGroup.addView(messageView, params);
        messageView.setVisibility(View.VISIBLE);
    }

    public void complete(String message) {
        complete(message, null);
    }

    public void complete(String message, Intent intent) {
        NotificationUtil.notifyDialog((AppCompatActivity) getActivity(), true, "Success", message, "OK", v -> {
            if (getActivity().getCallingActivity() != null) {
                if (intent != null)
                    getActivity().setResult(getActivity().RESULT_OK, intent);
                else
                    getActivity().setResult(getActivity().RESULT_OK);
                getActivity().finish();
            } else {
                getActivity().finish();
            }
        });
    }
}