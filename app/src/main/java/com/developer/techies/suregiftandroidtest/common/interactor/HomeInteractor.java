package com.developer.techies.suregiftandroidtest.common.interactor;

import android.arch.lifecycle.Lifecycle;
import android.view.View;

import com.developer.techies.suregiftandroidtest.common.presenter.AddHotelPresenter;

public class HomeInteractor {

    private View view;
    private Lifecycle lifecycle;

    AddHotelPresenter presenter;


    public HomeInteractor(View view, Lifecycle lifecycle){
        this.view = view;
        this.lifecycle = lifecycle;
        presenter = new AddHotelPresenter(view);
    }
}
