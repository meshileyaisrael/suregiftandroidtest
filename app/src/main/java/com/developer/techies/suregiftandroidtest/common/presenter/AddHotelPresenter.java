package com.developer.techies.suregiftandroidtest.common.presenter;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.util.FormViewPresenter;
import com.developer.techies.suregiftandroidtest.util.ValidatorUtil;
import com.parse.ParseACL;

public class AddHotelPresenter extends FormViewPresenter<HotelItems> {

    ImageView mPhotoView;

    EditText hotelName, hotelAmount,  hotelSize, hotelPaymentTime, hotelConfiguration, hotelRentDetails,
            hotelAvailableStatus, hotelAvailableFor, hotelFurnishingDetails, hotelAreaDetails, hotelSellersName, hotelSellersContact;

    public AddHotelPresenter(View view) {
        super(view);

        mPhotoView = view.findViewById(R.id.hotel_image);
        hotelName = view.findViewById(R.id.hotel_name);
        hotelAmount = view.findViewById(R.id.hotel_amount);
        hotelSize = view.findViewById(R.id.hotel_size);
        hotelPaymentTime = view.findViewById(R.id.hotel_payment_time);
        hotelConfiguration = view.findViewById(R.id.hotel_configuration);
        hotelRentDetails = view.findViewById(R.id.hotel_rent_details);
        hotelAvailableStatus = view.findViewById(R.id.hotel_available_status);
        hotelAvailableFor = view.findViewById(R.id.hotel_available_for);
        hotelFurnishingDetails = view.findViewById(R.id.hotel_furnishing_details);
        hotelAreaDetails = view.findViewById(R.id.hotel_area_details);
        hotelSellersName = view.findViewById(R.id.hotel_sellers_name);
        hotelSellersContact = view.findViewById(R.id.hotel_sellers_contact);

    }

    @Override
    public boolean isValid() {
        return ValidatorUtil.isValid(hotelName) & ValidatorUtil.isValid(hotelAmount)&ValidatorUtil.isValid(hotelSize)&ValidatorUtil.isValid(hotelPaymentTime)&ValidatorUtil.isValid(hotelConfiguration)
                &ValidatorUtil.isValid(hotelRentDetails)&ValidatorUtil.isValid(hotelAvailableStatus)&ValidatorUtil.isValid(hotelAvailableFor)&ValidatorUtil.isValid(hotelFurnishingDetails)&ValidatorUtil.isValid(hotelAreaDetails)
                &ValidatorUtil.isValid(hotelSellersContact)&ValidatorUtil.isValid(hotelSellersName);
    }

    @Override
    public HotelItems getUpdate() {
        HotelItems hotelItems = getData();

        if (hotelItems == null) {
            hotelItems = new HotelItems();
        }

        ParseACL publicAcl = new ParseACL();
        publicAcl.setPublicReadAccess(true);
        publicAcl.setPublicWriteAccess(true);
        hotelItems.setACL(publicAcl);

        hotelItems.setHotelName(hotelName.getText().toString());
        hotelItems.setHotelPrice(hotelAmount.getText().toString());
        hotelItems.setHotelSize(hotelSize.getText().toString());
        hotelItems.setPaymentTime(hotelPaymentTime.getText().toString());
        hotelItems.setHotelConfiguration(hotelConfiguration.getText().toString());
        hotelItems.setRentDetails(hotelRentDetails.getText().toString());
        hotelItems.setAvailableStatus(hotelAvailableStatus.getText().toString());
        hotelItems.setAvailableForStatus(hotelAvailableFor.getText().toString());
        hotelItems.setFurnishingDetails(hotelFurnishingDetails.getText().toString());
        hotelItems.setAreaDetails(hotelAreaDetails.getText().toString());
        return hotelItems;
    }

    public ImageView getmPhotoView() {
        return mPhotoView;
    }
}