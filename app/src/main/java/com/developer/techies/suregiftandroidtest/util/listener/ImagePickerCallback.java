package com.developer.techies.suregiftandroidtest.util.listener;

import android.graphics.Bitmap;

/**
 * Created by ribads on 10/24/17.
 */

public interface ImagePickerCallback {
    public void onPickerCancelled();
    public void onPickerDone(Bitmap scaledBitmap);
}
