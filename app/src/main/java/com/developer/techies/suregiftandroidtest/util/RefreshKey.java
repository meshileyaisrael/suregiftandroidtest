package com.developer.techies.suregiftandroidtest.util;

import android.content.Context;
import android.content.SharedPreferences;

public class RefreshKey {

    public static final String ACTIVITY = "etywd8g";
    public static final String CAREGIVER_INVITATION_KEY = "cg_hsgvjw223";
    public static final String CHILDREN = "child3783689(";
    public static final String CRECHE_CLASS = "class67357ha";
    public static final String STAFF = "jcj6dvju22";

    public static final String CAREGIVER_ACTIVITIES = "tdyg8i211";
    public static final String PAYMENT_METHOD = "jdebrt2387e23";

    public static final String ALD_LIST = "oe9djwwddj";
    public static final String ALD_ASPECT_LIST = "383euduue";
    public static final String COEL_ITEM = "eid123kdi";
    public static final String ASSESSMENT_LIST = "iridsoe4";
    public static final String MESSAGE_LIST = "irjrjs4";
    public static final String EYFS_ITEM = "lkteadd4";


    public static boolean clearKey(String key){
        Context context = ParseApplication.getInstance().getBaseContext();
        SharedPreferences settings = context.getSharedPreferences(PreferenceHelper.MAIN_PREF_NAME, 0);
        return settings.edit().remove(key).commit();
    }

}