package com.developer.techies.suregiftandroidtest.common.view.bottom_fragment;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appyvet.materialrangebar.RangeBar;
import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.util.BaseFragment;

public class UserFilterFragment extends BaseFragment {

    public UserFilterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_filter, container, false);
        initViews(view);
        initData();
        return view;
    }

    public void initViews(View view) {
        rangebar = (RangeBar) view.findViewById(R.id.rangebar1);
        final TextView leftIndexValue = (TextView) view.findViewById(R.id.leftIndexValue);
        final TextView rightIndexValue = (TextView) view.findViewById(R.id.rightIndexValue);

        rangebar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex, String leftPinValue, String rightPinValue) {
                leftIndexValue.setText("" + leftPinIndex);
                rightIndexValue.setText("" + rightPinIndex);
            }

        });
    }

    public void initData() {

    }

    // Sets variables to save the colors of each attribute
    private int mBarColor;


    private int mConnectingLineColor;

    // Initializes the RangeBar in the application
    private RangeBar rangebar;

    // Saves the state upon rotating the screen/restarting the activity
    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("BAR_COLOR", mBarColor);
        bundle.putInt("CONNECTING_LINE_COLOR", mConnectingLineColor);
    }
}


