package com.developer.techies.suregiftandroidtest.common.view.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.common.view.fragment.ContactOwnerFragment;
import com.developer.techies.suregiftandroidtest.common.widget.HeaderView;
import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.db.util.BaseQuery;
import com.developer.techies.suregiftandroidtest.util.BaseActivity;
import com.squareup.picasso.Picasso;

public class HotelDetailsActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener {

    String hotelId;
    HotelItems hotelItems;

    protected HeaderView toolbarHeaderView;
    protected HeaderView floatHeaderView;
    protected AppBarLayout appBarLayout;

    TextView nameTv;

    private boolean isHideToolbarView = false;

    ImageView hotelImage;

    TextView hotelLocation, hotelAmount, hotelSize, hotelPaymentTime, hotelConfiguration, hotelRentDetails,
            hotelAvailableStatus, hotelAvailableForDetails, hotelFurnishingDetails, hotelAreaDetails, hotelAddressDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_details);

        toolbarHeaderView = findViewById(R.id.toolbar_header_view);
        floatHeaderView = findViewById(R.id.float_header_view);
        appBarLayout = findViewById(R.id.appbar);

        initBar("");
        hotelId = getIntent().getExtras().getString("hotelId");
        hotelItems = BaseQuery.getObject(HotelItems.class, hotelId);

        initViews();
        initData();
    }

    public void initViews() {
        NestedScrollView scrollView = findViewById(R.id.scrollView);
        scrollView.setFillViewport(true);
        nameTv = findViewById(R.id.user_name);
        appBarLayout.addOnOffsetChangedListener(this);
        hotelImage = findViewById(R.id.hotel_image);
        hotelLocation = findViewById(R.id.hotel_location);
        hotelAmount = findViewById(R.id.hotel_amount);
        hotelSize = findViewById(R.id.hotel_size);
        hotelPaymentTime = findViewById(R.id.hotel_payment_time);
        hotelConfiguration = findViewById(R.id.hotel_configuration);
        hotelRentDetails = findViewById(R.id.rent_details);
        hotelAvailableStatus = findViewById(R.id.available_status);
        hotelAvailableForDetails = findViewById(R.id.available_for_details);
        hotelFurnishingDetails = findViewById(R.id.furnishing_details);
        hotelAreaDetails = findViewById(R.id.area_details);
        hotelAddressDetails = findViewById(R.id.address_details);
    }

    public void initData() {
        if (hotelItems != null) {
            if (hotelItems.getHotelImage() != null)
                Picasso.with(this).load(hotelItems.getHotelImage().getUrl()).into(hotelImage);
            hotelLocation.setText(hotelItems.getHotelAddress());
            hotelAmount.setText(hotelItems.getHotelPrice());
            hotelSize.setText(hotelItems.getHotelSize());
            hotelPaymentTime.setText(hotelItems.getPaymentTime());
            hotelConfiguration.setText(hotelItems.getHotelConfiguration());
            hotelRentDetails.setText(hotelItems.getRentDetails());
            hotelAvailableStatus.setText(hotelItems.getAvailableStatus());
            hotelAvailableForDetails.setText(hotelItems.getAvailableForStatus());
            hotelFurnishingDetails.setText(hotelItems.getFurnishingDetails());
            hotelAreaDetails.setText(hotelItems.getAreaDetails());
            hotelAddressDetails.setText(hotelItems.getHotelAddress());
            nameTv.setText(hotelItems.getHotelName());

            toolbarHeaderView.bindTo(hotelItems.getHotelName());
            floatHeaderView.bindTo("");
        }


    }

    public void onContactClick(View view) {
        ContactOwnerFragment dialog = new ContactOwnerFragment();
        dialog.setSellerInfo(hotelItems);
        dialog.show(getSupportFragmentManager(), null);

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            nameTv.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            nameTv.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;
        }

    }
}
