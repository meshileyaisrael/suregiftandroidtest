package com.developer.techies.suregiftandroidtest.db.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;

@ParseClassName("HotelItems")
public class HotelItems extends BaseObject {

    public void setCreator(User creator) {
        addUnique("creator", creator);
    }

    public User getCreator() {
        return (User) getParseObject("creator");
    }

    public void setHotelPrice(String price) {
        put("hotelPrice", price);
    }

    public String getHotelPrice() {
        return getString("hotelPrice");
    }

    public void setHotelAddress(String address) {
        put("hotelAddress", address);
    }

    public String getHotelAddress() {
        return getString("hotelAddress");
    }

    public void setHotelImage(ParseFile hotelImage) {
        put("hotelImage", hotelImage);
    }

    public ParseFile getHotelImage() {
        return getParseFile("hotelImage");
    }

    public void setHotelCategory(String category) {
        put("hotelCategory", category);
    }

    public String getHotelCategory() {
        return getString("hotelCategory");
    }

    public void setHotelName(String name) {
        put("hotelName", name);
    }

    public String getHotelName() {
        return getString("hotelName");
    }

    public void setHotelSize(String size) {
        put("hotelSize", size);
    }

    public String getHotelSize() {
        return getString("hotelSize");
    }

    public void setPaymentTime(String time) {
        put("paymentTime", time);
    }

    public String getPaymentTime() {
        return getString("paymentTime");
    }

    public void setHotelConfiguration(String configuration) {
        put("hotelConfiguration", configuration);
    }

    public String getHotelConfiguration() {
        return getString("hotelConfiguration");
    }

    public void setRentDetails(String details) {
        put("rentDetails", details);
    }

    public String getRentDetails() {
        return getString("rentDetails");
    }

    public void setAvailableStatus(String status) {
        put("hotelStatus", status);
    }

    public String getAvailableStatus() {
        return getString("hotelStatus");
    }

    public void setAvailableForStatus(String status) {
        put("availableForStatus", status);
    }

    public String getAvailableForStatus() {
        return getString("availableForStatus");
    }

    public void setFurnishingDetails(String details) {
        put("furnishingDetails", details);
    }

    public String getFurnishingDetails() {
        return getString("furnishingDetails");
    }

    public void setAreaDetails(String areaDetails) {
        put("areaDetails", areaDetails);
    }

    public String getAreaDetails() {
        return getString("areaDetails");
    }

    public void setSellersName(String name) {
        put("sellersName", name);
    }

    public String getSellersName() {
        return getString("sellersName");
    }

    public void setSellersContactNumber(String number) {
        put("sellersContact", number);
    }

    public String getSellersContact() {
        return getString("sellersContact");
    }
}

