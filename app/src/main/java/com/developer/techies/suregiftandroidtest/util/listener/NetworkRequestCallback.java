package com.developer.techies.suregiftandroidtest.util.listener;

/**
 * Created by ribads on 10/9/17.
 */

public interface NetworkRequestCallback {
    public void onRequestSuccess(String response);
    public void onRequestFailed(String message, Exception ex);
}