package com.developer.techies.suregiftandroidtest.util;

import android.widget.TextView;

public class ValidatorUtil {

    public static boolean isValid(TextView mTextView, int lenght, String errorMessage){
        boolean valid = mTextView.getText().toString().trim().length() > lenght;
        if (!valid)
            mTextView.setError(errorMessage);
        return valid;
    }

    public static boolean isValid(TextView mTextView){
        return isValid(mTextView, "Field required");
    }

    public static boolean isValid(TextView mTextView, String errorMessage){
        boolean valid = mTextView.getText().toString().trim().length() > 0;
        if (!valid)
            mTextView.setError(errorMessage);
        return valid;
    }

    public static boolean isEmailValid(TextView mTextView){
        String email = mTextView.getText().toString().trim();
        if (isEmailValid(email))
            return true;
        mTextView.setError("Invalid email address");
        return false;
    }

    public static boolean isEmailValid (String value){
        return (value.contains("@"));
    }

    public static boolean isValid(TextView mTextView, int minLenght){
        boolean valid = mTextView.getText().toString().trim().length() > minLenght;
        if (!valid)
            mTextView.setError("Value must be greater that "+minLenght+" digits");
        return valid;
    }


}
