package com.developer.techies.suregiftandroidtest.util;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.developer.techies.suregiftandroidtest.util.listener.ImagePickerCallback;
import com.parse.ParseFile;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class PickImage extends BaseActivity {

    public static final String TAG = "ImagePickerActivity";
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 809;
    private ParseFile pFile;
    private Bitmap scaledBitmap;
    private String compressedFilePath;
    private List<ImagePickerCallback> callbacks;

    public void attachImagePickerListener(ImagePickerCallback callback) {
        if (this.callbacks == null)
            this.callbacks = new ArrayList<>();
        if (!this.callbacks.contains(callback))
            this.callbacks.add(callback);
    }

    public void launchPicker() {
        if (hasPermission()) {
            startCropper();
        }
    }

    private void startCropper() {
        // start picker to get image for cropping and then use the image in cropping activity
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
//            if (shouldShowRequestPermissionRationale(
//                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                // Explain to the user why we need to read the contacts
//            }
//
//            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                            Manifest.permission.CAMERA},
//                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant

            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {
                    Log.w("", "Permission has been denied by user");
                } else {
                    this.startCropper();
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(imageReturnedIntent);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                processImageUri(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                onPickerCancelled();
            }
        }
    }

    private void onPickerCancelled() {
        if (this.callbacks != null && this.callbacks.size() > 0) {
            for (ImagePickerCallback callback : callbacks) {
                callback.onPickerCancelled();
            }
        }
    }

    private void onPickerDone() {
        if (this.callbacks != null && this.callbacks.size() > 0) {
            for (ImagePickerCallback callback : callbacks) {
                callback.onPickerDone(this.scaledBitmap);
            }
        }
    }

    private void processImageUri(Uri uri) {
        try {
            scaledBitmap = FileHelper.scaleDownImage(uri.getPath());
            compressedFilePath = FileHelper.getOutputMediaFileUri(FileHelper.MEDIA_TYPE_IMAGE).getPath();
            FileOutputStream out = new FileOutputStream(compressedFilePath);
            //write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
            File file = new File(compressedFilePath);
            FileInputStream inputStream;
            try {
                inputStream = new FileInputStream(file);
                byte[] data = new byte[inputStream.available()];
                inputStream.read(data);
                pFile = new ParseFile(data, "image/jpeg");
                pFile.saveInBackground();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ArithmeticException ex) {
            ex.printStackTrace();
            NotificationUtil.displayToast(this, "Invalid Image Selected");
        }
        onPickerDone();
    }

    public String getCompressedFilePath() {
        return compressedFilePath;
    }

    public void setCompressedFilePath(String compressedFilePath) {
        this.compressedFilePath = compressedFilePath;
    }

    public ParseFile getParseFile() {
        return pFile;
    }

    public void setParseFile(ParseFile file) {
        this.pFile = file;
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Destroy teh file here
        FileHelper.destroyTempFile(compressedFilePath);
    }

}
