package com.developer.techies.suregiftandroidtest.common.view.activity.googlesearch;

//public class CurrentLocationNearByPlacesActivity extends BaseActivity{
//
//    public static final String TAG = "CurrentLocNearByPlaces";
//    private static final int LOC_REQ_CODE = 1;
//
//    protected GeoDataClient geoDataClient;
//    protected PlaceDetectionClient placeDetectionClient;
//    protected RecyclerView recyclerView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.curent_nearby);
//
////        Toolbar tb = findViewById(R.id.toolbar);
////        setSupportActionBar(tb);
////        tb.setSubtitle("Near By Places");
//
//        recyclerView = findViewById(R.id.places_lst);
//
//        LinearLayoutManager recyclerLayoutManager =
//                new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(recyclerLayoutManager);
//
//        DividerItemDecoration dividerItemDecoration =
//                new DividerItemDecoration(recyclerView.getContext(),
//                        recyclerLayoutManager.getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration);
//
//        placeDetectionClient = Places.getPlaceDetectionClient(this, null);
//
//        getCurrentPlaceItems();
//
//    }
//
//    private void getCurrentPlaceItems() {
//        if (isLocationAccessPermitted()) {
//            getCurrentPlaceData();
//        } else {
//            requestLocationAccessPermission();
//        }
//    }
//
//    @SuppressLint("MissingPermission")
//    private void getCurrentPlaceData() {
//        Task<PlaceLikelihoodBufferResponse> placeResult = placeDetectionClient.
//                getCurrentPlace(null);
//        placeResult.addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
//            @Override
//            public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
//                Log.d(TAG, "current location places info");
//                List<Place> placesList = new ArrayList<Place>();
//                PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
//                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
//                    placesList.add(placeLikelihood.getPlace().freeze());
//                }
//                likelyPlaces.release();
//
//                PlacesRecyclerViewAdapter recyclerViewAdapter = new
//                        PlacesRecyclerViewAdapter(placesList,
//                        CurrentLocationNearByPlacesActivity.this);
//                recyclerView.setAdapter(recyclerViewAdapter);
//            }
//        });
//    }
//
//    private boolean isLocationAccessPermitted() {
//        if (ContextCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//            return false;
//        } else {
//            return true;
//        }
//    }
//
//    private void requestLocationAccessPermission() {
//        ActivityCompat.requestPermissions(this,
//                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                LOC_REQ_CODE);
//    }
//
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == LOC_REQ_CODE) {
//            if (resultCode == RESULT_OK) {
//                getCurrentPlaceData();
//            }
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.places_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.view_places_m:
//                getCurrentPlaceItems();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
//}
