package com.developer.techies.suregiftandroidtest.util.listener;

/**
 * Created by STANLEY on 1/19/2018.
 */

public interface OnLongItemClickListener <T> {
    public boolean onLongItemClick(T t);
}
