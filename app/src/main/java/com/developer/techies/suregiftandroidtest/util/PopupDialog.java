package com.developer.techies.suregiftandroidtest.util;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;

public class PopupDialog extends DialogFragment {

    private View.OnClickListener listener;

    public static PopupDialog newInstance(boolean isSuccess, String title, String message, String buttonTitle) {
        PopupDialog popupDialog = new PopupDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("message", message);
        bundle.putBoolean("success", isSuccess);
        if (buttonTitle != null)
            bundle.putString("button", buttonTitle);
        popupDialog.setArguments(bundle);
        return popupDialog;
    }

    public PopupDialog() {

    }

    public void attachListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_popup, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        TextView mTitleView = view.findViewById(R.id.title);
        TextView mMessageView = view.findViewById(R.id.sub_title);
        Button button = view.findViewById(R.id.action_button);

        if (getArguments() != null) {
            String title = getArguments().getString("title");
            String message = getArguments().getString("message");
            String buttonTitle = getArguments().getString("button");

            ValueUtil.initTextView(mTitleView, title);
            ValueUtil.initTextView(mMessageView, message);
            ValueUtil.initTextView(button, buttonTitle);

            boolean isSuccess = getArguments().getBoolean("success");
            button.setBackgroundResource(isSuccess ? R.drawable.bg_button_flat_green : R.drawable.bg_button_flat_red);
        }

        button.setOnClickListener(v -> {
            if (listener != null)
                listener.onClick(v);
            dismiss();
        });
    }
}

