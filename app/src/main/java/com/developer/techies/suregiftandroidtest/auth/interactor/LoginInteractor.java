package com.developer.techies.suregiftandroidtest.auth.interactor;

import android.view.View;

import com.developer.techies.suregiftandroidtest.auth.presenter.LoginPresenter;
import com.parse.LogInCallback;
import com.parse.ParseUser;


public class LoginInteractor {

    LoginPresenter viewPresenter;
    LogInCallback callback;

    public LoginInteractor(View view, LogInCallback callback){
        this.viewPresenter = new LoginPresenter(view);
        this.callback = callback;
    }

    public boolean login(){
        if (viewPresenter.isValid()){
            String email = viewPresenter.getEmail();
            String password = viewPresenter.getPassword();
            ParseUser.logInInBackground(email, password, callback);
            return true;
        }
        else {
            return false;
        }
    }

}
