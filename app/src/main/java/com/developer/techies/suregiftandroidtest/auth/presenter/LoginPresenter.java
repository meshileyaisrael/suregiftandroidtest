package com.developer.techies.suregiftandroidtest.auth.presenter;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.FormViewPresenter;
import com.developer.techies.suregiftandroidtest.util.ValidatorUtil;


public class LoginPresenter extends FormViewPresenter<User> {

    private EditText emailTv;
    private EditText passwordTv;
    private Button mSubmitButton;
    private TextInputLayout mPasswordLayout;

    public LoginPresenter(View view) {
        super(view);
        emailTv = view.findViewById(R.id.username_field);
        passwordTv = view.findViewById(R.id.password_field);
        mSubmitButton = view.findViewById(R.id.email_auth);
        mPasswordLayout = view.findViewById(R.id.password_layout);
        mPasswordLayout.setPasswordVisibilityToggleEnabled(true);
    }

    @Override
    public boolean isValid() {
        return ValidatorUtil.isEmailValid(emailTv) & ValidatorUtil.isValid(passwordTv, 4, "Invalid Password");
    }

    @Override
    public void bindData(User toddlrUser) {
        throw new UnsupportedOperationException("This isn't required");
    }

    @Override
    public User getUpdate() {
        return null;
    }

    public Button getmSubmitButton() {
        return mSubmitButton;
    }

    public EditText getEmailTv() {
        return emailTv;
    }

    public EditText getPasswordTv() {
        return passwordTv;
    }

    public String getEmail() {
        return this.emailTv.getText().toString().trim().toLowerCase();
    }

    public String getPassword() {
        return this.passwordTv.getText().toString();
    }
}
