package com.developer.techies.suregiftandroidtest.auth.activity;

import android.util.Log;

import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.NotificationUtil;
import com.developer.techies.suregiftandroidtest.util.listener.FetchOperationListener;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.HashMap;
import java.util.Map;

import bolts.Continuation;
import bolts.Task;


public class GoogleAuthHelper implements Continuation<ParseUser, Void>, SaveCallback {

    private FetchOperationListener<User> listener;
    private String userType;
    private GoogleSignInAccount acct;
    private String email;

    public GoogleAuthHelper(FetchOperationListener<User> listener) {
        this.listener = listener;
    }

    public void setRole(String toddlrUserType) {
        this.userType = toddlrUserType;
    }

    public void createUser(GoogleSignInAccount acct) {
        if (acct != null) {
            this.acct = acct;
            Map<String, String> authData = new HashMap<String, String>();
            authData.put("id", acct.getId());
            authData.put("access_token", acct.getIdToken());
            Task<ParseUser> t = ParseUser.logInWithInBackground("google", authData);
            t.continueWith(this);
        }
    }


    @Override
    public Void then(Task<ParseUser> task) throws Exception {
        if (task.isCancelled()) {
            Log.w("Login", "Task cancelled");
            this.done(new ParseException(-111, "Google authentication operation cancelled"));
        } else if (task.isFaulted()) {
            Log.w("Login", "Save FAIL" + task.getError());
            this.done(new ParseException(-111, "Google authentication operation failed"));
        } else {
            User pUser = (User) task.getResult();
            this.email = acct.getEmail();
            pUser.setEmail(acct.getEmail());
            pUser.setFirstName(acct.getFamilyName());
            pUser.setLastName((acct.getGivenName()));
//            pUser.setRole("USER");
            pUser.setGooglePhotoURL(acct.getPhotoUrl().toString());
            pUser.saveInBackground(this);
        }
        return null;
    }

    @Override
    public void done(ParseException e) {
        if (e == null) {
          //  IntercomHelper.registerUser();
            listener.onResult(User.getCurrentUser(), null);
        } else if (User.getCurrentUser() != null) {
            User pUser = User.getCurrentUser();
            Log.e("Authentication", String.valueOf(e.getCode()));
            if (e.getCode() == ParseException.EMAIL_TAKEN) {
                pUser.deleteInBackground(e1 -> listener.onResult(null, new Exception(e1 == null ? "An account already exist for the user with this email address ("+this.email+"). Please login into your account" : "An error has occurred. Please try again later")));
            } else {
                NotificationUtil.displayToast("An error has occurred. Please try again later");
                pUser.deleteInBackground(e1 -> listener.onResult(null, new Exception(e1 == null ? "An account already exist for the user with this email address ("+this.email+"). Please login into your account" : "An error has occurred. Please try again later")));
            }
        } else {
            listener.onResult(null, e);
        }
    }
}
