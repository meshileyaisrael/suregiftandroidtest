package com.developer.techies.suregiftandroidtest.util.worker;

import android.arch.lifecycle.Lifecycle;

import com.developer.techies.suregiftandroidtest.util.PreferenceHelper;
import com.developer.techies.suregiftandroidtest.util.listener.FetchOperationListener;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QueryWorker<T extends ParseObject> extends BaseWorker<ParseQuery<T>, Integer, List<T>> {

    FetchOperationListener<List<T>> listener;
    private String refreshKey;

    public QueryWorker(Lifecycle lifecycle, String refreshKey) {
        super(lifecycle);
        setRefreshKey(refreshKey);
    }

    public void attachListener(FetchOperationListener<List<T>> listener) {
        this.listener = listener;
    }

    public void setRefreshKey(String key) {
        this.refreshKey = key;
    }

    @Override
    protected List<T> doInBackground(ParseQuery<T>... queries) {
        try {
            List<T> lists = new ArrayList<T>();
            for (ParseQuery<T> query : queries) {
                Date date = this.refreshKey != null ? PreferenceHelper.getDateUpdate(this.refreshKey) : null;
                if (date != null)
                    query.whereGreaterThan("updatedAt", date);
                Date requestTime = new Date();
                List<T> result = query.find();
                if (result != null && result.size() > 0) {
                    PreferenceHelper.setDateUpdate(refreshKey, requestTime);
                    T.pinAll(result);
                    lists.addAll(result);
                }
            }
            return lists;
        } catch (Exception ex) {
            setException(ex);
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<T> ts) {
        super.onPostExecute(ts);
        if (listener != null)
            listener.onResult(ts, getException());
    }

    @Override
    protected void stop() {
        this.cancel(true);
        this.listener = null;
    }
}
