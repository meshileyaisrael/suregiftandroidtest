package com.developer.techies.suregiftandroidtest.db.helper;

import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.db.util.BaseQuery;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

public class HotelsHelper {

    public ParseQuery<HotelItems> getQuery() {
        ParseQuery<HotelItems> query = BaseQuery.getQuery(HotelItems.class);
        query.orderByDescending("updatedAt");
        return query;
    }

    public List<HotelItems> getHotels() {
        ParseQuery<HotelItems> query = this.getQuery();
        query.fromLocalDatastore();
        try {
            return query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
