package com.developer.techies.suregiftandroidtest.auth.worker;

import android.arch.lifecycle.Lifecycle;
import android.os.Bundle;
import android.util.Log;

import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.listener.SaveOperationListener;
import com.developer.techies.suregiftandroidtest.util.worker.BaseWorker;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.parse.ParseFacebookUtils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FacebookAuthWorker extends BaseWorker<Void, Void, Exception> {

    User user = User.getCurrentUser();
    private static final String TAG = "FacebookAuthWorker";
    private SaveOperationListener callback;

    public FacebookAuthWorker(Lifecycle lifecycle, SaveOperationListener fetchCallback) {
        super(lifecycle);
        this.callback = fetchCallback;
    }

    public void initData() throws Exception {
        if (ParseFacebookUtils.isLinked(user)) {
            Log.e(TAG, "Calling the Linked User");

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,email,birthday");

            GraphRequest profileGraphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), null);
            profileGraphRequest.setParameters(parameters);

            Bundle params = new Bundle();
            params.putBoolean("redirect", false);
            GraphRequest profilePhotoRequest = new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "me/picture?type=large",
                    params,
                    HttpMethod.GET, null);

            GraphRequestBatch batchRequest = new GraphRequestBatch(profileGraphRequest, profilePhotoRequest);
            List<GraphResponse> responses = batchRequest.executeAndWait();
            GraphResponse profileResponse = responses.get(0);
            if (profileResponse.getJSONObject() != null) {
                processMeJSONObject(profileResponse.getJSONObject());
            } else {
                Log.e(TAG, "Error: " + profileResponse.getError().toString());
            }

            GraphResponse profilePhotoResponse = responses.get(1);

            Log.e(TAG, profilePhotoResponse.toString());
            String picUrlString = (String) profilePhotoResponse.getJSONObject().getJSONObject("data").get("url");
            user.setFacebookPhotoURL(picUrlString);
            user.save();
        }
    }

    public void processMeJSONObject(JSONObject resultJSON) {
        try {
            Log.e(TAG, resultJSON.toString());
            String firstName = resultJSON.getString("first_name");
            if (firstName != null) {
                user.setFirstName(firstName);
            }
            String lastName = resultJSON.getString("last_name");
            if (lastName != null) {
                user.setLastName(lastName);
            }

            String email = resultJSON.getString("email");
            if (email != null) {
                user.setEmail(email);
                user.setRole("USER");
            }

            String birthDay = resultJSON.getString("birthday");
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date date = formatter.parse(birthDay);
            if (date != null) {
                user.setDateOfBirth(date);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected Exception doInBackground(Void... params) {
        try {
            initData();
        } catch (Exception e) {
            e.printStackTrace();
            return e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Exception e) {
        super.onPostExecute(e);
        if (callback != null)
            callback.onSave(e);
    }

    @Override
    protected void stop() {
        this.callback = null;
    }
}
