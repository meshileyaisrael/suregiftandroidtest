package com.developer.techies.suregiftandroidtest.util;

import android.view.View;

public abstract class FormViewPresenter<T> extends ViewPresenter {

    private T data;

    public FormViewPresenter(View view) {
        super(view);
    }

    public abstract boolean isValid();
    public void bindData(T t){
        this.data = t;
    }

    protected T getData() {
        return data;
    }

    public abstract T getUpdate();

}
