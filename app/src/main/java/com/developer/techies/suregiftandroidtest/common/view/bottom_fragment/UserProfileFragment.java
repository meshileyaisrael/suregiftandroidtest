package com.developer.techies.suregiftandroidtest.common.view.bottom_fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.PickImageFragment;
import com.developer.techies.suregiftandroidtest.util.listener.ImagePickerCallback;
import com.developer.techies.suregiftandroidtest.util.listener.SaveOperationListener;
import com.developer.techies.suregiftandroidtest.util.worker.SavingWorker;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.squareup.picasso.Picasso;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class UserProfileFragment extends PickImageFragment implements ImagePickerCallback, SaveOperationListener {

    ImageView imageView;
    EditText nameField;
    EditText locationField;
    EditText emailField;

    Button saveButton;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        initViews(view);
        initData();
        return view;
    }

    private void initViews(View view) {
        imageView = view.findViewById(R.id.profile_pic);
        nameField = view.findViewById(R.id.name_field);
        locationField = view.findViewById(R.id.location_field);
        emailField = view.findViewById(R.id.email_field);
        saveButton = view.findViewById(R.id.save_button);
        this.attachImagePickerListener(this);
    }

    public void initData() {
        User user = User.getCurrentUser();
        nameField.setText(user.getLastName());
        if (user.getPhoto() != null)
            Picasso.with(getActivity()).load(user.getPhoto()).placeholder(R.drawable.default_user_profile).into(imageView);
        locationField.setText(user.getLocation());
        emailField.setText(user.getEmail());

        imageView.setOnClickListener(v -> photoOnClick());
        locationField.setOnClickListener( v ->  onSearchLocation());
        saveButton.setOnClickListener(v -> updateProfile());

    }

    public void onSearchLocation() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(getActivity());
            startActivityForResult(intent, 1);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }
    public void updateProfile() {
        User user = User.getCurrentUser();
        if (getParseFile() != null)
            user.setPhoto(getParseFile());
        user.setLastName(nameField.getText().toString());
        user.setLocation(locationField.getText().toString());
        user.setEmail(emailField.getText().toString());

        showProgress("Updating profile...");
        updateProfile(user, this);
    }

    public void updateProfile(User user, SaveOperationListener listener) {
        SavingWorker<User> worker = new SavingWorker<>(getLifecycle());
        worker.attachListener(listener);
        worker.execute(user);
    }

    public void photoOnClick() {
        launchPicker();
    }

    @Override
    public void onPickerCancelled() {

    }

    @Override
    public void onPickerDone(Bitmap scaledBitmap) {
        imageView.setImageBitmap(scaledBitmap);

    }

    @Override
    public void onSave(Exception ex) {
        this.hideProgress();
        if (ex != null) {
            showErrorInformation("An error has occurred. Please try again");
            ex.printStackTrace();
        } else {
            Intent intent = new Intent();
            complete("Profile was updated successfully...", intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                locationField.setText(place.getAddress().toString());

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
                Log.e("Tag", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
