package com.developer.techies.suregiftandroidtest.auth.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.auth.activity.GoogleAuthHelper;
import com.developer.techies.suregiftandroidtest.auth.interactor.LoginInteractor;
import com.developer.techies.suregiftandroidtest.auth.worker.FacebookAuthWorker;
import com.developer.techies.suregiftandroidtest.common.view.activity.MainActivity;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.BaseActivity;
import com.developer.techies.suregiftandroidtest.util.Config;
import com.developer.techies.suregiftandroidtest.util.NotificationUtil;
import com.developer.techies.suregiftandroidtest.util.listener.FetchOperationListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

import java.util.Arrays;

public class LoginActivity extends BaseActivity implements LogInCallback, FetchOperationListener<User> {

    private static final String TAG = "LoginActivity";
    private static final int RC_SIGN_IN = 9001;
    LoginInteractor interactor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        interactor = new LoginInteractor(findViewById(android.R.id.content), this);
    }

    public void onCancel(View view){
//        Intent start = new Intent(this, WelcomeActivity.class);
//        startActivity(start);
    }

    public void onLoginClick(View view) {
        if (interactor.login()) {
            showProgress("Signing into account");
        }
    }

    public void onCreateAccountClick(View view){
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    public void onFacebookClick(View view) {
        String[] permissions = {"email", "public_profile", "user_birthday", "user_friends"};
        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, Arrays.asList(permissions), new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                    err.printStackTrace();
                    Log.e(TAG, "Uh oh. The user cancelled the Facebook login.");
                } else if (user.isNew()) {
                    Log.e(TAG, "User signed up and logged in through Facebook!");
                    onFacebookSuccess();
                } else {
                    Log.e(TAG, "User logged in through Facebook!");
                    onFacebookSuccess();
                }
            }
        });
    }
    private void onFacebookSuccess() {

        showProgress("Initializing...");
        FacebookAuthWorker worker = new FacebookAuthWorker(getLifecycle(), ex -> {
            if (ex == null) {
//                IntercomHelper.registerUser();
                LaunchClass(this, MainActivity.class, null, true);
            } else {
                this.hideProgress();
                User user = User.getCurrentUser();
                if (user != null) {
                    user.deleteInBackground(e -> Log.e(TAG, e == null ? "The account has been deleted successfully" : "Account delete not successful"));
                }
                if (ex instanceof ParseException) {
                    ParseException parseException = (ParseException) ex;
                    if (parseException.getCode() == ParseException.EMAIL_TAKEN || parseException.getCode() == ParseException.USERNAME_TAKEN) {
                        NotificationUtil.notifyDialog(this, false, "Facebook authentication failed", "It appears that an account already exists with the email address connected to this facebook account. Please login into your account");
                        return;
                    }
                }
                NotificationUtil.notifyDialog(this, false, "Facebook authentication failed", "The facebook authentication failed. Please try again later");
            }
        });
        worker.execute();
    }

    GoogleSignInClient mGoogleSignInClient;

    public void onGoogleClick(View view) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Config.GOOGLE_SERVICE_CLIENT_ID)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            GoogleAuthHelper helper = new GoogleAuthHelper(this);
            this.showProgress("Setting up...");
            helper.createUser(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            e.printStackTrace();
            Log.w("TAG", "signInResult:failed code=" + e.getMessage());
            this.showErrorInformation("An error has occurred. Please try again later");
        }
    }

    @Override
    public void onResult(User toddlrUser, Exception ex) {
        hideProgress();
        if (ex != null) {
            String errorMessage = ex.getMessage();
            NotificationUtil.notifyDialog(this, false, "Google authentication failed", errorMessage);
            mGoogleSignInClient.signOut();
        } else {
            LaunchClass(this, MainActivity.class, null, true);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void done(ParseUser user, ParseException e) {
        hideProgress();
        if (user != null) {
//            LaunchClass(this);
            LaunchClass(this, MainActivity.class, null, false);
        } else {
            showAuthErrorInformation(e);
        }
    }

    public void onForgotPasswordClick(View view) {
        //LaunchClass(this, ForgotPasswordActivity.class, null, false);
    }
}
