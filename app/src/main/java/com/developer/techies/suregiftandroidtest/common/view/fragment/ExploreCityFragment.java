package com.developer.techies.suregiftandroidtest.common.view.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.util.BaseFragment;

public class ExploreCityFragment extends BaseFragment {

    public ExploreCityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_explore_city, container, false);
        initViews(view);
        initData();
        return view;
    }

    private void initViews(View view) {
    }

    public void initData(){

    }
}
