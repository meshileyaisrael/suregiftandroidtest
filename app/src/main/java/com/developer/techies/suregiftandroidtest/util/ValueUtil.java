package com.developer.techies.suregiftandroidtest.util;

import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;


public class ValueUtil {

    public static String getString(TextView mTextView) {
        String value = mTextView.getText().toString().trim();
        return value;
    }

    public static String getEmail(TextView mTextView) {
        String value = mTextView.getText().toString().trim();
        return value.toLowerCase();
    }

    public static int getInt(TextView mTextView) {
        try {
            Integer value = Integer.parseInt(mTextView.getText().toString().trim());
            return value;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public static void initTextView(TextView mTextView, String value){
        if (mTextView != null && value != null){
            mTextView.setText(value);
        }
    }

    public static Calendar getCalendar(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

}

