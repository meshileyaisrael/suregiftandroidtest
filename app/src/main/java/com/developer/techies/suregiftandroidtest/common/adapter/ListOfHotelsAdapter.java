package com.developer.techies.suregiftandroidtest.common.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.common.adapter.holder.ListOfHotelsHolder;
import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.util.listener.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListOfHotelsAdapter extends RecyclerView.Adapter<ListOfHotelsHolder> {

    private OnItemClickListener<HotelItems> listener;
    List<HotelItems> hotelItems = Collections.emptyList();
    List<HotelItems> allhotels = new ArrayList<>();

    public ListOfHotelsAdapter(List<HotelItems> hotelItems) {
        if (hotelItems != null)
            allhotels = hotelItems;
        this.hotelItems = hotelItems;
    }

    public void attachListener(OnItemClickListener<HotelItems> listener) {
        this.listener = listener;
    }

    public void replaceItems(List<HotelItems> hotelItems) {
        this.hotelItems = hotelItems;
        this.notifyDataSetChanged();
    }

    public void filter(String text) {
        hotelItems.clear();

        if (text.isEmpty()) {
            hotelItems.addAll(allhotels);
        } else {
            if (allhotels != null && allhotels.size() > 0){
                text = text.toLowerCase();
                for (HotelItems item : allhotels) {
                    if (item.getHotelAddress().toLowerCase().contains(text) || item.getHotelAddress().toLowerCase().contains(text)) {
                        hotelItems.add(item);
                    }
                }
            }

        }
        notifyDataSetChanged();
    }

    @Override
    public ListOfHotelsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_hotels, parent, false);
        return new ListOfHotelsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListOfHotelsHolder holder, int position) {
        HotelItems model = hotelItems.get(position);
        holder.hotelPrice.setText(model.getHotelPrice());
        holder.hotelAddress.setText(model.getHotelAddress());

        if (model.getHotelImage() != null)
            Picasso.with(holder.itemView.getContext()).load(model.getHotelImage().getUrl()).placeholder(R.drawable.icon_email).into(holder.hotelImage);
        holder.itemView.setOnClickListener(v -> listener.onItemClick(model));
    }

    @Override
    public int getItemCount() {
        return hotelItems != null ? hotelItems.size() : 0;
    }
}
