package com.developer.techies.suregiftandroidtest.auth.view;

import android.os.Bundle;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.common.view.activity.MainActivity;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.BaseActivity;
import com.developer.techies.suregiftandroidtest.util.LogoutHelper;
import com.developer.techies.suregiftandroidtest.util.NotificationUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.parse.GetCallback;
import com.parse.ParseException;


public class SplashActivity extends BaseActivity implements GetCallback<User> {
    private static final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        analyticsTrack();

        User user = User.getCurrentUser();
        if (user != null)
            user.fetchInBackground(this);
        else
            LaunchClass(this, LoginActivity.class, null, true);
    }

    @Override
    public void done(User user, ParseException e) {
        if (user != null) {
            user.updateLastLoginAsync();
            LaunchClass(this, MainActivity.class, null, true);
            //IntercomHelper.registerUser();
        }
        else {
            NotificationUtil.notifyDialog(this , false, "Error", "An error has occurred and " +
                    "you would need to re-login into your account to continue", "Continue", v -> LogoutHelper.Logout(this));
        }
    }

    public void analyticsTrack() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "App Opened");
        FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle);
    }

}
