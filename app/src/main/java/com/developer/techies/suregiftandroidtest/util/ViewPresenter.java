package com.developer.techies.suregiftandroidtest.util;

import android.content.Context;
import android.view.View;

public abstract class ViewPresenter {

    public View itemView;
    public Context context;
    public ViewPresenter(View itemView){
        this.itemView = itemView;
    }

}
