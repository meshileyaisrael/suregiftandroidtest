package com.developer.techies.suregiftandroidtest.common.model;

import android.arch.lifecycle.Lifecycle;

import com.developer.techies.suregiftandroidtest.db.helper.HotelsHelper;
import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.util.listener.FetchOperationListener;
import com.developer.techies.suregiftandroidtest.util.worker.QueryWorker;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import com.parse.ParseQuery;

import java.util.List;

public class HomeFragmentModel {

    private HotelsHelper helper;
    private Lifecycle lifecycle;

    public HomeFragmentModel(Lifecycle lifecycle) {
        helper = new HotelsHelper();
        this.lifecycle = lifecycle;
    }

    public void fetch(FetchOperationListener<List<HotelItems>> listener) {
        ParseQuery<HotelItems> query = helper.getQuery();

        QueryWorker<HotelItems> worker = new QueryWorker<>(this.lifecycle, null);
        worker.attachListener(listener);
        worker.execute(query);
    }

    public List<HotelItems> getHotelList() {
        return this.helper.getHotels();
    }
}
