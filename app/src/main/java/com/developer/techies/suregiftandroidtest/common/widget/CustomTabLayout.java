package com.developer.techies.suregiftandroidtest.common.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.developer.techies.suregiftandroidtest.R;


public class CustomTabLayout extends TabLayout {

    private Typeface typeface;

    public CustomTabLayout(Context context) {
        super(context);
        initTypeface(null);
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTypeface(attrs);
    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTypeface(attrs);
    }

    private void initTypeface(AttributeSet attrs) {
        if (attrs == null) {
            this.typeface = Typeface.createFromAsset(getContext().getAssets(), getContext().getString(R.string.font_medium));
        } else {
            TypedArray a = getContext().obtainStyledAttributes(
                    attrs,
                    R.styleable.CustomTabLayout);
            String path = a.getString(
                    R.styleable.CustomTabLayout_fontPath);
            if (path == null) {
                this.typeface = Typeface.createFromAsset(getContext().getAssets(), getContext().getString(R.string.font_medium));
            }
            this.typeface = Typeface.createFromAsset(getContext().getAssets(), path);
        }
    }

    @Override
    public void setTabsFromPagerAdapter(@NonNull PagerAdapter adapter) {

        this.removeAllTabs();
        ViewGroup slidingTabStrip = (ViewGroup) getChildAt(0);

        for (int i = 0, count = adapter.getCount(); i < count; i++) {
            Tab tab = this.newTab();
            this.addTab(tab.setText(adapter.getPageTitle(i)));
            AppCompatTextView view = (AppCompatTextView) ((ViewGroup) slidingTabStrip.getChildAt(i)).getChildAt(1);
            view.setTypeface(typeface, Typeface.NORMAL);
        }
    }

    @Override
    public void setupWithViewPager(ViewPager viewPager) {

        super.setupWithViewPager(viewPager);
        if (typeface != null) {
            this.removeAllTabs();

            ViewGroup slidingTabStrip = (ViewGroup) getChildAt(0);

            PagerAdapter adapter = viewPager.getAdapter();

            for (int i = 0, count = adapter.getCount(); i < count; i++) {
                Tab tab = this.newTab();
                this.addTab(tab.setText(adapter.getPageTitle(i)));
                AppCompatTextView view = (AppCompatTextView) ((ViewGroup) slidingTabStrip.getChildAt(i)).getChildAt(1);
                view.setTypeface(typeface, Typeface.NORMAL);
            }
        }
    }
}