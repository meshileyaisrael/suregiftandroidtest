package com.developer.techies.suregiftandroidtest.common.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.db.model.User;

public class ContactOwnerFragment extends DialogFragment {

    private HotelItems user;

    TextView buyersName, sellersName, sellersContact;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_contact_owner, container, false);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme);
        initViews(view);
        initData();
        return view;
    }

    public void setSellerInfo(HotelItems user) {
        this.user = user;
    }

    private void initViews(View view) {
        buyersName = view.findViewById(R.id.buyers_name);
        sellersName = view.findViewById(R.id.sellers_name);
        sellersContact = view.findViewById(R.id.sellers_contact);
        view.findViewById(R.id.call_button).setOnClickListener((bView) -> onCallClick());
    }

    private void initData(){
        sellersName.setText(user.getSellersName());
        buyersName.setText(User.getCurrentUser().getLastName());
        sellersContact.setText(user.getSellersContact());
    }

    public void onCallClick(){
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + user.getSellersContact()));
        startActivity(intent);
        dismiss();
    }
}