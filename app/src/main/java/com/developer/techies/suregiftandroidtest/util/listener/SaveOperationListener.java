package com.developer.techies.suregiftandroidtest.util.listener;

/**
 * Created by ribads on 9/26/17.
 */

public interface SaveOperationListener {
    void onSave(Exception ex);
}
