package com.developer.techies.suregiftandroidtest.common.view.bottom_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SearchView;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.common.adapter.ListOfHotelsAdapter;
import com.developer.techies.suregiftandroidtest.common.model.HomeFragmentModel;
import com.developer.techies.suregiftandroidtest.common.view.activity.HotelDetailsActivity;
import com.developer.techies.suregiftandroidtest.db.model.HotelItems;
import com.developer.techies.suregiftandroidtest.util.BaseFragment;
import com.developer.techies.suregiftandroidtest.util.listener.OnItemClickListener;

import java.util.List;

public class HomeFragment extends BaseFragment implements OnItemClickListener<HotelItems> {

    RecyclerView recyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ListOfHotelsAdapter adapter;
    HomeFragmentModel viewModel;

    SearchView searchMenu;

    List<HotelItems> hotelItems;

    public HomeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initData();
    }

    private void initViews(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
        searchMenu = view.findViewById(R.id.search_menu_field);
        searchMenu.setIconifiedByDefault(false);
        searchMenu.setFocusable(true);
        searchMenu.requestFocus();
        searchMenu.setQueryHint("Type Location");

        int id = searchMenu.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = searchMenu.findViewById(id);
        searchEditText.setHint("Search list");
        searchEditText.setHintTextColor(R.color.grey_tint);
//        mSwipeRefreshLayout = view.findViewById(R.id.swipe_timeline_recycler);
//        mSwipeRefreshLayout.setOnRefreshListener(() -> fetchActivitiesFromRemote());
//        timelineAdapter = new TimelineAdapter(this, true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
        recyclerView.setHasFixedSize(true);
    }

    public void initContentData() {
        searchMenu.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText != null)
                    adapter.filter(newText);
                return true;
            }
        });
    }

    public void initData() {
        this.viewModel = new HomeFragmentModel(getLifecycle());

        hotelItems = viewModel.getHotelList();

        if (hotelItems == null || hotelItems.size() == 0) {
            initEmptyState(R.drawable.icon_email, "No Hotels added yet", null);
        }
        adapter = new ListOfHotelsAdapter(hotelItems);
        adapter.attachListener(this);
        recyclerView.setAdapter(adapter);
        onRefresh();
        initContentData();

    }

    private void onRefresh() {

        this.viewModel.fetch((hotelItems, ex) -> {
            if (hotelItems != null && hotelItems.size() > 0) {
                hideEmptyView();
                adapter.replaceItems(hotelItems);
            }
        });
    }

    @Override
    public void onItemClick(HotelItems hotelItems) {
        Intent intent = new Intent(getActivity(), HotelDetailsActivity.class);
        intent.putExtra("hotelId", hotelItems.getObjectId());
        startActivity(intent);

    }
}