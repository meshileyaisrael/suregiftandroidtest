package com.developer.techies.suregiftandroidtest.util;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class NotificationUtil {

    public static void displayToast(String message) {
        displayToast(ParseApplication.getInstance(), message);
    }

    public static void displayToast(Context context, String message) {
        if (context != null)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void displayToast(Context context, String message, int duration) {
        if (context != null)
            Toast.makeText(context, message, duration).show();
    }

    public static void notifyDialog(AppCompatActivity activity, boolean isSuccess, String title, String message) {
        PopupDialog popupDialog = PopupDialog.newInstance(isSuccess, title, message, null);
        popupDialog.show(activity.getSupportFragmentManager(), null);
    }

    public static void notifyDialog(AppCompatActivity activity, boolean isSuccess, String title, String message, String buttonTitle, View.OnClickListener listener) {
        PopupDialog popupDialog = PopupDialog.newInstance(isSuccess, title, message, buttonTitle);
        popupDialog.attachListener(listener);
        popupDialog.show(activity.getSupportFragmentManager(), null);

    }

    public static void notifyDialog(AppCompatActivity activity, boolean isSuccess, String title, String message, View.OnClickListener listener) {
        PopupDialog popupDialog = PopupDialog.newInstance(isSuccess, title, message, null);
        popupDialog.attachListener(listener);
        popupDialog.show(activity.getSupportFragmentManager(), null);
    }
}

