package com.developer.techies.suregiftandroidtest.auth.view;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.util.BaseActivity;
import com.developer.techies.suregiftandroidtest.util.ValidatorUtil;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;


public class ForgotPasswordActivity extends BaseActivity implements RequestPasswordResetCallback {

    EditText mEmailView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_forgot_password);

        initViews();
    }

    private void initViews() {
        mEmailView = findViewById(R.id.email);
    }

    private boolean isValid() {
        return ValidatorUtil.isEmailValid(mEmailView);
    }

    public void onRequestPasswordClick(View view) {
        if (isValid()) {
            String email = mEmailView.getText().toString().trim().toLowerCase();

            showProgress("Requesting password reset...");
            ParseUser.requestPasswordResetInBackground(email, this);
        }
    }

    @Override
    public void done(ParseException e) {
        hideProgress();
        if (e == null) {
            complete("Message");
        }
    }
}
