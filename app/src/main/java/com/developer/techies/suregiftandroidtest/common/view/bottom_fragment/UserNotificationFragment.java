package com.developer.techies.suregiftandroidtest.common.view.bottom_fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.util.BaseFragment;

public class UserNotificationFragment extends BaseFragment {

    public UserNotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_notification, container, false);
        initViews(view);
        initData();
        return view;
    }

    public void initViews(View view){

    }

    public void initData(){

    }
}
