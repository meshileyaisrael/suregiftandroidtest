package com.developer.techies.suregiftandroidtest.util.listener;

/**
 * Created by ribads on 9/26/17.
 */

public interface OnItemClickListener<T> {
    public void onItemClick(T t);
}
