package com.developer.techies.suregiftandroidtest.util;


import com.developer.techies.suregiftandroidtest.BuildConfig;

public class Config {
//

    public static String PARSE_APPLICATION_ID = "8656yjhfhyrfvjcxgdfcjkjvbhl0000000!";
    public static String PARSE_CLIENT_KEY = "12345tgbnki765432345tgcsedcdfc000000000!";

    public static final String DEMO_BASE_URL = "https://test-parse-server-test.herokuapp.com/";
    public static final String LIVE_BASE_URL = "https://test-parse-server-test.herokuapp.com/";

    public static final String GOOGLE_SERVICE_CLIENT_ID = "1013476194732-3q3tqlrrv4iqhl5i56a03jkv5ie0v02j.apps.googleusercontent.com";

    private static String getBaseServerURL() {
        if (BuildConfig.DEBUG) {
            return DEMO_BASE_URL;
        } else {
            return LIVE_BASE_URL;
        }
    }

    public static String getServerURL() {
        return getBaseServerURL() + "parse/";
    }

    public static String getReportURL() {
        return getBaseServerURL() + "report";
    }

//    public static String getSocketServer(){
//        if (BuildConfig.DEBUG){
//            return DEMO_SOCKET_SERVER;
//        }
//        else {
//            return LIVE_SOCKET_SERVER;
//        }
//    }
}
