package com.developer.techies.suregiftandroidtest.db.model;

import com.parse.ParseObject;

public class BaseObject extends ParseObject {
    public void remove() {
        this.put("removed", true);
    }
}
