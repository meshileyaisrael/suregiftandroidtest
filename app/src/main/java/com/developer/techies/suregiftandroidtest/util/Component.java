
package com.developer.techies.suregiftandroidtest.util;

public enum Component {
    BAR_COLOR, CONNECTING_LINE_COLOR, PIN_COLOR, THUMB_COLOR_PRESSED, SELECTOR_COLOR,SELECTOR_BOUNDARY_COLOR, TICK_COLOR, TEXT_COLOR
}
