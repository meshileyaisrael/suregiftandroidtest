package com.developer.techies.suregiftandroidtest.util.worker;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

public abstract class  BaseWorker<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> implements LifecycleObserver {

    private Exception exception;

    public BaseWorker(@NonNull Lifecycle lifecycle){
        lifecycle.addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    protected abstract void stop();

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
