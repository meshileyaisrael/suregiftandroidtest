package com.developer.techies.suregiftandroidtest.auth.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.auth.presenter.RegistrationPresenter;
import com.developer.techies.suregiftandroidtest.common.view.activity.MainActivity;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.developer.techies.suregiftandroidtest.util.BaseActivity;
import com.developer.techies.suregiftandroidtest.util.NotificationUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;


public class RegistrationActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "RegistrationActivity";
    RegistrationPresenter viewPresenter;
    private User user;
    private String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initViews();
        initData();
    }

    private void initData() {

    }

    private void openWebPage(String urlString) {
        Uri webpage = Uri.parse(urlString);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            NotificationUtil.displayToast(this, "No Application to Handle operation");
        }
    }
    private void initViews() {
        viewPresenter = new RegistrationPresenter(findViewById(android.R.id.content));
    }

    void onSuccess() {
        if (this.user != null && this.user.getRole() != null && this.user.getRole() == "Admin"){
            LaunchClass(this, MainActivity.class, null, false);
        }
        else {
            LaunchClass(this, MainActivity.class, null, false);
        }
    }


    public void onSubmitClick(View view) {
        if (viewPresenter.isValid()) {
            this.user = viewPresenter.getUpdate();
            showProgress("Creating account...");
            user.signUpInBackground(e -> {
                hideProgress();
                if (e == null) {
                  //  IntercomHelper.registerUser();
                    onSuccess();
                } else {
                    showAuthErrorInformation(e);
                    e.printStackTrace();
                }
            });
        }
    }

    public void onLoginClick(View view){
        LaunchClass(this, LoginActivity.class, null, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed:" + connectionResult);
    }
}
