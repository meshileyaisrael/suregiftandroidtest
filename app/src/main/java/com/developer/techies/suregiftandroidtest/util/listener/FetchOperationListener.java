package com.developer.techies.suregiftandroidtest.util.listener;

/**
 * Created by ribads on 9/27/17.
 */

public interface FetchOperationListener<T> {
    public void onResult(T t, Exception ex);

}
