package com.developer.techies.suregiftandroidtest.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.developer.techies.suregiftandroidtest.auth.view.LoginActivity;
import com.developer.techies.suregiftandroidtest.db.model.User;
import com.google.firebase.messaging.FirebaseMessaging;
import com.parse.ParseObject;

import static com.developer.techies.suregiftandroidtest.util.BaseActivity.LaunchClass;

public class LogoutHelper {

    public static void Logout(Context context){
        try{
            FirebaseMessaging.getInstance().unsubscribeFromTopic(User.getCurrentUser().getRole());
            InstallationHelper.HandleLogout(context);
            deletePref(context);
            User.logOutInBackground();
            ParseObject.unpinAllInBackground();
          //  IntercomHelper.unregisterUser();
            LaunchClass(context, LoginActivity.class, null, true);
        }catch (Exception e){
            NotificationUtil.displayToast("An unknown error occured!");
        }

    }

    public static void deletePref(Context context){
        SharedPreferences settings = context.getSharedPreferences(PreferenceHelper.MAIN_PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear().commit();
    }

}
