package com.developer.techies.suregiftandroidtest.util.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.util.adapter.holder.PickerItemHolder;
import com.developer.techies.suregiftandroidtest.util.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;


public class PickerAdapter extends RecyclerView.Adapter<PickerItemHolder> {

    ArrayList<String> items;
    OnItemClickListener<Integer> listener;
    private List<Integer> selectedIndies;
    boolean multiple = false;
    public static final String TAG = "PickerAdapter";

    public PickerAdapter(ArrayList<String> items, OnItemClickListener<Integer> listener) {
        this.selectedIndies = new ArrayList<>();
        this.items = items;
        this.listener = listener;
    }

    public PickerAdapter(ArrayList<String> items, List<String> selectedItems, OnItemClickListener<Integer> listener) {
        this.items = items;
        this.listener = listener;

        this.selectedIndies = new ArrayList<Integer>();
        updateIndies(selectedItems);
    }

    @Override
    public PickerItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_picker_item, parent, false);
        return new PickerItemHolder(view);
    }

    public void updateIndies(List<String> selectedItems) {
        if (selectedItems != null && selectedItems.size() > 0) {
            for (int i = 0; i < selectedItems.size(); i++) {
                int index = items.indexOf(selectedItems.get(i));
                if (index > 0)
                    selectedIndies.add(index);
            }
        }
    }

    public List<Integer> getSelectedIndies() {
        return selectedIndies;
    }

    public void setSelectedIndies(List<Integer> selectedIndies) {
        this.selectedIndies = selectedIndies;
    }

    public void setMultiple(boolean value) {
        this.multiple = value;
    }

    @Override
    public void onBindViewHolder(final PickerItemHolder holder, final int position) {
        holder.mItemView.setText(items.get(position));
        holder.mItemView.setTextColor(multiple ? holder.itemView.getContext().getResources().getColor(android.R.color.darker_gray)
                : holder.itemView.getContext().getResources().getColor(android.R.color.black));
        if (multiple && selectedIndies != null && selectedIndies.size() > 0) {
            int index = selectedIndies.indexOf(position);
            holder.mItemView.setTextColor(index > -1 ? holder.itemView.getContext().getResources().getColor(android.R.color.black) :
                    holder.itemView.getContext().getResources().getColor(android.R.color.darker_gray));
        }

        holder.itemView.setOnClickListener(v -> {
            if (multiple) {
                int selectedIndex = selectedIndies.indexOf(position);
                boolean selected = selectedIndex > -1;
                holder.mItemView.setTextColor(selected
                        ? v.getContext().getResources().getColor(android.R.color.black)
                        : v.getContext().getResources().getColor(android.R.color.black));
                if (selected) {
                    selectedIndies.remove(selectedIndex);
                } else {
                    selectedIndies.add(position);
                }
            } else if (listener != null)
                listener.onItemClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}