package com.developer.techies.suregiftandroidtest.common.view.activity;

import android.content.Intent;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.common.view.bottom_fragment.UserFilterFragment;
import com.developer.techies.suregiftandroidtest.common.view.bottom_fragment.UserNotificationFragment;
import com.developer.techies.suregiftandroidtest.common.view.bottom_fragment.UserProfileFragment;
import com.developer.techies.suregiftandroidtest.common.view.fragment.HomeMainFragment;
import com.developer.techies.suregiftandroidtest.db.model.User;

public class MainActivity extends AppCompatActivity {

    private Fragment fragment;

    FloatingActionButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_container, new HomeMainFragment());
        tx.commit();

        initViews();
    }

    public void onAddHotel(View view){
        Intent intent = new Intent(this, AddHotelActivity.class);
        startActivity(intent);
    }

    public void initViews() {
        User user = new User().getCurrentUser();
        button = findViewById(R.id.add_hotel_button);
        if (user != null && user.getRole() != null && user.getRole().equals("Admin")) {
            button.setVisibility(View.VISIBLE);
        }else{
            button.setVisibility(View.GONE);
        }

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.action_home:
                                setTitle("HOME");
                                fragment = new HomeMainFragment();
                                break;
                            case R.id.action_filter:
                                setTitle("News");
                                fragment = new UserFilterFragment();
                                break;
                            case R.id.action_notify:
                                fragment = new UserNotificationFragment();
                                break;
                            case R.id.action_profile:
                                setTitle("Profile");
                                fragment = new UserProfileFragment();
                                break;
                            default:
                                fragment = new HomeMainFragment();
                                break;
                        }


                        if (fragment != null) {
                            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.main_container, fragment).commit();
                            return true;
                        }
                        return false;
                    }
                });
        bottomNavigationView.getMenu().getItem(0).setChecked(true);

    }

    public void onAddHotel() {
        Intent intent = new Intent(this, UserProfileFragment.class);
        startActivity(intent);

    }
}
