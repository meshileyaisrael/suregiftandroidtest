package com.developer.techies.suregiftandroidtest.util.worker;

import android.arch.lifecycle.Lifecycle;

import com.developer.techies.suregiftandroidtest.util.listener.SaveOperationListener;
import com.parse.ParseObject;

import java.util.Arrays;
import java.util.List;

public class SavingWorker<T extends ParseObject> extends BaseWorker<T, Integer, Exception> {

    SaveOperationListener listener;
    public SavingWorker(Lifecycle lifecycle) {
        super(lifecycle);
    }

    public void attachListener(SaveOperationListener listener){
        this.listener = listener;
    }

    @Override
    protected void stop() {
        this.cancel(true);
        this.listener = null;
    }

    @Override
    protected Exception doInBackground(T[] ts) {
        try {
            List<T> tArray = Arrays.asList(ts);
            T.saveAll(tArray);
            T.pinAll(tArray);
            return null;
        } catch (Exception ex) {
            setException(ex);
            return ex;
        }
    }

    @Override
    protected void onPostExecute(Exception ex) {
        super.onPostExecute(ex);
        if (listener != null)
            listener.onSave(ex);
    }
}
