package com.developer.techies.suregiftandroidtest.common.view.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.techies.suregiftandroidtest.R;
import com.developer.techies.suregiftandroidtest.common.adapter.HomePagerAdapter;
import com.developer.techies.suregiftandroidtest.util.BaseFragment;

public class HomeMainFragment extends BaseFragment {

    TabLayout mTabLayout;
    ViewPager mViewPager;

    HomePagerAdapter adapter;

    public HomeMainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_home_main, container, false);
        initViews(view);
        initData();
        return view;
    }


    public void initViews(View view){
        mTabLayout = view.findViewById(R.id.tabLayout);
        mViewPager = view.findViewById(R.id.viewPager);
    }

    private void initData() {
        adapter = new HomePagerAdapter(getFragmentManager());
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
