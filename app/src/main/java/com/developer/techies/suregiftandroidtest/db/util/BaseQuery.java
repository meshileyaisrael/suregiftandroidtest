package com.developer.techies.suregiftandroidtest.db.util;

import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

public class BaseQuery<T extends ParseObject> {

    public static <T extends ParseObject> ParseQuery<T> getQuery(Class<T> subclass) {
        return new ParseQuery<>(subclass).whereNotEqualTo("removed", true);
    }

    public static <T extends ParseObject> T getObject(Class<T> subclass, String id, String[] includes) {
        try {
            ParseQuery<T> query = getQuery(subclass);
            query.fromLocalDatastore();
            if (includes != null && includes.length >0){
                for (String include : includes){
                    query.include(include);
                }
            }
            return query.get(id);
        } catch (Exception ex) {
            return null;
        }
    }
    public static <T extends ParseObject> T getObject(Class<T> subclass, String id) {
        return getObject(subclass, id, null);
    }

    public static <T extends ParseObject> List<T> getObjects(Class<T> subclass, List<String> includes) {
        try {
            ParseQuery<T> query = getQuery(subclass);
            query.fromLocalDatastore();
            if (includes != null && includes.size() > 0)
                for (String key : includes) {
                    query.include(key);
                }
            return query.find();
        } catch (Exception ex) {
            return null;
        }
    }
}
