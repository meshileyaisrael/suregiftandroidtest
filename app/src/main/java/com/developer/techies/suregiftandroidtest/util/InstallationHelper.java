package com.developer.techies.suregiftandroidtest.util;

import android.content.Context;

import com.parse.ParseInstallation;

public class InstallationHelper {

    public static final String TAG = "InstallationHelper";
    public static String CHILDREN_KEY = "children";
    public static String USER_KEY = "user";
    public static String CRECHE_KEY = "creche";
    public static String FCM_TOKEN = "fcm_token";
    public static String CRECHE_CLASSES_KEY = "crecheClasses";
    //public static String CRECHE_CLASS_KEY = "crecheClass";
    public static String ROLE_KEY = "role";

    public static void HandleLogout(Context context) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.remove(CHILDREN_KEY);
        installation.remove(CRECHE_KEY);
        installation.remove(CRECHE_CLASSES_KEY);
        installation.saveInBackground();
    }

}
